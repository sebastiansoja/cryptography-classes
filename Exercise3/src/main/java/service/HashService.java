package service;

import model.User;
import org.spongycastle.crypto.digests.SHA256Digest;
import org.spongycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.spongycastle.crypto.params.KeyParameter;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.*;
import java.util.*;

@Service
public class HashService {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:Database.db";
    private Connection conn;
    private Statement stat;
    private static final Random RANDOM = new SecureRandom();
    private static final int ITERATIONS = 10000;
    private static final int KEY_LENGTH = 256;


    public HashService() throws SQLException {
        try {
            Class.forName(HashService.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }

        createTables();
    }

    /**
     * @throws SQLException wynik funkcji w przypadku bledu zapytania
     *     funkcja tworzaca tabele JESLI ta nie istnieje
     */
    public  void createTables() throws SQLException {
        String createUsers="CREATE TABLE IF NOT EXISTS Users(id INTEGER PRIMARY KEY AUTOINCREMENT, username varchar(30), password varchar(30)," +
                " salt varchar(30) )";
        try {
            stat.execute(createUsers);
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
        }
        finally {
            stat.close();
        }
    }

    /**
     * @param username uzytkownik podaje nazwe
     * @param password podane zostaje haslo
     * @throws SQLException wynik funkcji w przypadku błędu zapytania
     * wynikiem funkcji jest dodanie uzytkownika (jego nazwy, hasla i wylosowanej soli) do bazy
     */
    public  void insertUser(String username, String password) throws SQLException {
        PreparedStatement prepStmt = conn.prepareStatement(
                "insert into Users(username, password, salt) values (?, ?, ?);");
        prepStmt.setString(1, username);
        prepStmt.setString(2, hash(password.toCharArray(), getNextSalt()));
        prepStmt.setString(3, getNextSalt());
        prepStmt.execute();
    }

    /**
     * @param password uzytkownik podaje haslo
     * @param salt tworzona zostaje losowa sol
     */
    public  String pbkdf2(String password, String salt) {
        PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA256Digest());
        byte[] secretData = password.getBytes();
        byte[] saltData = salt.getBytes();
        gen.init(secretData, saltData, ITERATIONS);
        byte[] derivedKey = ((KeyParameter)gen.generateDerivedParameters(KEY_LENGTH * 8)).getKey();
        return toHex(derivedKey);
    }

    private  String toHex(byte[] bytes) {
        BigInteger bi = new BigInteger(1, bytes);
        return String.format("%0" + (bytes.length << 1) + "x", bi);
    }

    /**
     * @return funkcja losujaca sol, encodujaca ją na typ String
     */
    public static String getNextSalt() {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }

    /**
     * @param password podane zostaje haslo
     * @param salt  podana zostaje losowa sol
     * @return Funkcja zwraca shashowane hasło na podstawie podanego hasla przez uzytkownika i wylosowanej soli
     */
    public static String hash(char[] password, String salt) {
        PBEKeySpec spec = new PBEKeySpec(password, Base64.getDecoder().decode(salt), ITERATIONS, KEY_LENGTH);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return Base64.getEncoder().encodeToString(skf.generateSecret(spec).getEncoded());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Błąd podczas hashowania hasla: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }

    /**
     *
     * @param password     hasło do sprawdzenia
     * @param salt         sól uzywana do hashowania hasła
     * @param expectedHash oczekiwany hash dla hasła
     *
     * @return true jezeli haslo i sól sa rowne hashowanej wartosci, w przeciwnym razie false
     */
    public static boolean isExpectedPassword(char[] password, String salt, char[] expectedHash) {
        char[] pwdHash = hash(password, salt).toCharArray();
        Arrays.fill(password, Character.MIN_VALUE);
        if (pwdHash.length != expectedHash.length) return false;
        for (int i = 0; i < pwdHash.length; i++) {
            if (pwdHash[i] != expectedHash[i]) return false;
        }
        return true;
    }


    /**
   @return funkcja zwracajaca wszystkich uzytkownikow w bazie
     */
    public List<User> selectUsers() {
        List<User> users = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM Users");
            Long id;
            String username, password, salt;
            while(result.next()) {
                id = result.getLong("id");
                username = result.getString("username");
                password = result.getString("password");
                salt = result.getString("salt");
                users.add(new User( id, username, password, salt));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return users;
    }
}
