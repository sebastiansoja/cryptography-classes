import model.User;
import service.HashService;

import java.sql.SQLException;
import java.util.List;

public class JdbcTest {

    public static void main(String[] args) throws SQLException {
        HashService h = new HashService();

        h.insertUser("sebastian", "sebastian");
        h.insertUser("jan", "kowalski");
        List<User> users = h.selectUsers();

        System.out.println("Lista czytelników: ");
        for(User userModel: users)
            System.out.println(userModel);

        boolean checkIf = HashService.isExpectedPassword("password".toCharArray(), HashService.getNextSalt(),
                HashService.hash("password".toCharArray(), HashService.getNextSalt()).toCharArray());

    }


}
