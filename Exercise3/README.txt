# Opis programu 

Program zapisuje w bazie danych nazwę użytkownika oraz zahashowane hasło wraz z solą
# Działanie programu

Program generuje tablę Users w przypadku gdy jeszcze nie istnieje. 
Użytkownik wprowadza swoje dane, program następnie korzysta z pbkdf2_hmac PBKDF2WithHmacSHA1 SecretKey. 
Generowana jest losowa sól. 
Następnie otrzymane dane wpisywane są do tabeli. 
Funkcja isExpectedPassword sprawdza hasło
